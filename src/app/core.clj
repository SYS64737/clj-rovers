(ns app.core
  (:require [app.parser :as parser]))

(defn turn-left [{:keys [orient] :as rover-pos}]
  "Turn a rover 90 degrees counter-clockwise"
  (->> (condp = orient
         :N :W
         :W :S
         :S :E
         :E :N)
       (assoc rover-pos :orient)
       (hash-map :updated-pos)
       (into {:cmd-trail :L})))

(defn turn-right [{:keys [orient] :as rover-pos}]
  "Turn a rover 90 degrees clockwise"
  (->> (condp = orient
         :N :E
         :E :S
         :S :W
         :A :X
         :W :N)
       (assoc rover-pos :orient)
       (hash-map :updated-pos)
       (into {:cmd-trail :R})))

(defn within-boundaries? [{grid-x :x  grid-y :y}
                          {rover-x :x rover-y :y}]
  "Verify a given rover hansn't fallen outside the grid boundaries"
  (and (<= 0 rover-x grid-x)
       (<= 0 rover-y grid-y)))

(defn not-overlapping-with-any-other-rover [other-rovers-pos {:keys [x y]}]
  "Check a given rover is not overlapping with any of the other rovers"
  (not (some #(and (= (:x %) x)
                   (= (:y %) y)) other-rovers-pos)))

(defn safety-checks-ok? [context rover-pos]
  (and (within-boundaries? (:grid-top-right context) rover-pos)
       (not-overlapping-with-any-other-rover
         (:other-rovers-pos context) rover-pos)))

(defn move-forward [context rover-pos]
  "Update x/y coordinates accordingly to NSWE orientation.
   Invalidate command (trail placeholder marked with a _ char)
   if any safety check fails"
  (let [new-rover-pos (condp = (:orient rover-pos)
                        :N (update rover-pos :y inc)
                        :E (update rover-pos :x inc)
                        :S (update rover-pos :y dec)
                        :W (update rover-pos :x dec))]
    (if (safety-checks-ok? context new-rover-pos)
      {:updated-pos new-rover-pos
       :cmd-trail :M}
      {:updated-pos rover-pos
       :cmd-trail :_})))

(defn exec-command [context position command]
  "Execute the given command on the given position
   making sure safety checks are respected"
  (condp = command
    :L (turn-left position)
    :R (turn-right position)
    :M (move-forward context position)))

(defn all-commands-executed [{:keys [commands trail]}]
  "all commands have been executed once there is
   a matching trail marker for each command"
  (>=
   (count trail)
   (count commands)))

(defn next-command-for [{:keys [commands trail]}]
  "The next command to execute is the first one missing a matching trail"
  (get commands (count trail)))

(defn step-rover [context rover]
  "If there are pending commands pick and execute the next one"
  (if (all-commands-executed rover)
    rover
    (let [{:keys [updated-pos cmd-trail]}
          (->> rover
               (next-command-for)
               (exec-command context (:pos rover)))]
      (assoc rover
             :pos updated-pos
             :trail (conj
                     (:trail rover)
                     cmd-trail)
             :position-history (conj
                                (:position-history rover)
                                updated-pos)))))

(defn send-heartbeat [{:keys [grid-top-right rovers] :as state}]
  "Cycle through all rovers and execute the next command (if any left)"
  (loop [prev-rovers rovers
         new-rovers []]
    (if (empty? prev-rovers)
      (assoc state :rovers new-rovers)
      (let [rover (first prev-rovers)
            other-rovers-pos (concat
                              (map :pos (rest prev-rovers))
                              (map :pos new-rovers))
            context {:grid-top-right grid-top-right
                     :other-rovers-pos other-rovers-pos}]
        (recur (rest prev-rovers)
               (conj new-rovers
                     (step-rover context rover)))))))

(defn- are-there-pending-commands? [{:keys [rovers]}]
  "Is there any robot left with not executed commands?"
  (let [has-pending-commands  #(not= (count (:commands %))
                                     (count (:trail %)))]
    (some has-pending-commands rovers)))

(defn- init-state [{:keys [grid-top-right rovers]}]
  "Decorate the input with the additional trail vector"
  {:grid-top-right grid-top-right
   :rovers (map #(into % {:trail []
                          :position-history [(:pos %)]})
                rovers)})

(defn run [input]
  (->> input
       (init-state)
       (iterate send-heartbeat)
       (drop-while are-there-pending-commands?)
       first))
