(ns app.parser
  [:require [clojure.string :as str]])

(defn- parse-plateau-size [line]
  (let [[x y] (rest (re-find #"(\d)+ (\d)+" line))]
    {:x (Integer/parseInt x) :y (Integer/parseInt y)}))

(defn parse-start-pos [line]
  (let [[x y orient] (rest (re-find #"(\d)+ (\d)+ (\w)" line))]
    {:x (Integer/parseInt x) :y (Integer/parseInt y) :orient (keyword orient)}))

(defn- to-keys [char-seq]
  (mapv (fn [c] (keyword (str c))) char-seq))

(defn- parse-rover [rover-str]
  {:pos (->> rover-str first parse-start-pos)
   :commands (->> rover-str second seq to-keys)})

(defn- parse-size-and-rovers [input-str]
  (let [grid-top-right-line (first input-str)
        rovers-lines (partition 2 (rest input-str))]
    {:grid-top-right (parse-plateau-size grid-top-right-line)
     :rovers (map parse-rover rovers-lines)}))

(defn is-not-commented [line]
  (not (.startsWith line ";")))

(defn parse-file [input-file]
    (->> (slurp input-file)
         (str/split-lines)
         (filter is-not-commented)
         (parse-size-and-rovers)))
