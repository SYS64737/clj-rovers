(ns app.command-line
  (:require [app.parser :as parser]
            [app.core :as core]
            [clojure.string :as str]))

(defn pformat-output [output]
  (let [format-history (fn [h] (str (:x h) " "
                                    (:y h) " "
                                    (:orient h)))
        format-rover #(str
                       "\n\tPosition: " (:pos %)
                       "\n\tCommands: " (apply str (->> % :commands (map name)))
                       "\n\tTrail:    " (apply str (->> % :trail (map name)))
                       "\n\tPosition History:"
                       (str/join "|" (->> %
                                          :position-history
                                          (map format-history))))]
    (str "  Rovers:"
         (str/join "\n"
                   (mapv format-rover (:rovers output))))))


(defn -main [& args]
  (if (seq args)
    (let [input-file (first args)
          input (parser/parse-file input-file)]
      (println "--")
      (println "Input: \n" (slurp input-file))
      (println "--")
      (println "Output: \n" (pformat-output (core/run input))))
    (throw (Exception. "Must provide input file name"))))
