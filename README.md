# Problem statement
A squad of robotic rovers are to be landed by NASA on a plateau on Mars.
This plateau, which is curiously rectangular, must be navigated by the rovers
so that their on-board cameras can get a complete view of the surrounding
terrain to send back to Earth.

A rover’s position is represented by a combination of
* x and y co-ordinates
* a letter representing one of the four cardinal compass points (N)orth/(S)outh/(E)ast/(W)est for camera direction

The plateau is divided up into a grid to simplify navigation.
An example position might be 0, 0, N, which means the rover is in the bottom left corner and facing North.

In order to control a rover, NASA sends a simple string of letters.
The possible letters are ‘L’, ‘R’ and ‘M’.
‘L’ and ‘R’ makes the rover spin 90 degrees left or right respectively, without moving from its current spot.
‘M’ means move forward one grid point, and maintain the same heading.

Assume that the square directly North from (x, y) is (x, y+1).

### Provided

* a plateau grid size, specified by the top right coordinates of the grid, e.g. (4,5)
* a list of robots each having:
  * a starting position, e.g. (0,0) North
  * a sequence of commands (letters) for each robot, e.g. MMRMMM

### **Calculate the final position of each rover**, e.g. 1 2 E**

# Example input
    4 5
    0 0 N
    MMRMMM
    3 4 W
    MMMLMMMLLM



### Input explation:

  - A 4x5 grid having:
  - one robot starting from the (0,0) cell, pointing North, required to execute 2 move forward commands, one turn right command and 3 final move forward commands
  - a second robot starting from the (3,4) cell ...etctc

# Example output
    1 2 E
    0 2 N

### Output explanation:

  - the first robot, after executing the 6 given commands, ended up in cell (1,2) pointing towards East
  - the second robot, after executing the 10 given commands, ended up in cell (0,2) pointing towards North

# Problem extentions

Implement these two additional extentions:

* a rover should not fall outside the grid boundaries (defined by all cells from (0,0) to (TopX,TopY))
* the rovers execute one command each in rounds and they will skip any command that could lead to a state with two rovers in the same cell (overlapping/crashing on each others). Report the final rover execution plan in order to be able to see, side by side with the original provided plan, which commands were skipped


# Provided solution

It's a commandline application that can be run with one of the input[0..2].txt files, e.g.:

    lein run input0.txt

Example run:

    lein run input2.txt
    --
    Input:
    ;; the two rovers will block each other after the fifth move
    ;; The second rover will then turn left (East), move forward and solve the lock (second rover doesn't have time for this stubborn rover one)
    6 6
    0 0 N
    MMMMMMMMM
    0 6 S
    MMMMMLMMM

    --
    Output:
      Rovers:
      Position: {:x 0, :y 5, :orient :N}
      Commands: MMMMMMMMM
      Trail:    MMM____MM
      Position History:0 0 :N|0 1 :N|0 2 :N|0 3 :N|0 3 :N|0 3 :N|0 3 :N|0 3 :N|0 4 :N|0 5 :N

      Position: {:x 3, :y 4, :orient :E}
      Commands: MMMMMLMMM
      Trail:    MM___LMMM
      Position History:0 6 :S|0 5 :S|0 4 :S|0 4 :S|0 4 :S|0 4 :S|0 4 :E|1 4 :E|2 4 :E|3 4 :E


The entry point is _command_line.clj_, the main logic is in _core.clj_ while the _parser.clj_ does ... its job

(each file has a number of rows in the power of two)